# pacmanAgents.py
# ---------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from pacman import Directions
from game import Agent
from heuristics import *
import random
import math
import pydron
import threading


class RandomAgent(Agent):
    # Initialization Function: Called one time when the game starts
    def registerInitialState(self, state):
        return;

    # GetAction Function: Called with every frame
    def getAction(self, state):
        # get all legal actions for pacman
        actions = state.getLegalPacmanActions()
        # returns random action from all the valide actions
        return actions[random.randint(0, len(actions) - 1)]


class RandomSequenceAgent(Agent):
    # Initialization Function: Called one time when the game starts
    def registerInitialState(self, state):
        self.actionList = [];
        for i in range(0, 10):
            self.actionList.append(Directions.STOP);
        return;

    # GetAction Function: Called with every frame
    def getAction(self, state):
        # get all legal actions for pacman
        possible = state.getAllPossibleActions();
        for i in range(0, len(self.actionList)):
            self.actionList[i] = possible[random.randint(0, len(possible) - 1)];
        tempState = state;
        for i in range(0, len(self.actionList)):
            if tempState.isWin() + tempState.isLose() == 0:
                tempState = tempState.generatePacmanSuccessor(self.actionList[i]);
            else:
                break;
        # returns random action from all the valide actions
        return self.actionList[0];


class GreedyAgent(Agent):
    # Initialization Function: Called one time when the game starts
    def registerInitialState(self, state):
        return;

    # GetAction Function: Called with every frame
    def getAction(self, state):
        # get all legal actions for pacman
        legal = state.getLegalPacmanActions()
        # get all the successor state for these actions
        successors = [(state.generatePacmanSuccessor(action), action) for action in legal]
        # evaluate the successor states using scoreEvaluation heuristic
        scored = [(scoreEvaluation(state), action) for state, action in successors]
        # get best choice
        bestScore = max(scored)[0]
        # get all actions that lead to the highest score
        bestActions = [pair[1] for pair in scored if pair[0] == bestScore]
        # return random action from the list of the best actions
        return random.choice(bestActions)





class MCTSNode:
    parent = None

    def __init__(self, pa):
        self.parent = pa
        self.reward = 0.0
        self.visited = 0
        self.children = {}  # 'South': NextNode, 'North': NextNode


def NODE_PRINT(mctsNode, state, nodeName=""):
    if nodeName is not "":
        print "Node Name: ", nodeName
    print "Node reward: ", mctsNode.reward
    print "Node visited: ", mctsNode.visited
    print "Node Children: ", mctsNode.children
    print "Node parent: ", mctsNode.parent
    print "Node state: \n", state


class MCTSAgent(Agent):
    # Initialization Function: Called one time when the game starts
    def registerInitialState(self, state):
        self.constant = 1.0  # Use 1 as the constant between exploitation and exploration.
        self.root = None
        self.state = None
        return

    def treePolicy(self, mctsNode):
        v = mctsNode
        while self.state is not None and \
                not self.state.isLose() and \
                not self.state.isWin() and v is not None:
            actions = self.state.getLegalPacmanActions()
            for action in actions:
                if action not in v.children:
                    children = self.expand(v, action)
                    if children is None:
                        continue
                    v.children[action] = children
                    return v.children[action]
            if v.children == {}:
                return None
            if self.state is not None and \
                    not self.state.isLose() and \
                    not self.state.isWin():
                v = self.select(v, self.constant)
        return None

    
    def expand(self, mctsNode, action):
	#Start thread here
        nextState = self.state.generatePacmanSuccessor(action)
        if nextState is None or nextState.isLose():
            return None
        self.state = nextState
        return MCTSNode(mctsNode)

   
    def select(self, mctsNode, constant):
        def selection_formula(parent, children, constant):
            return (children.reward / children.visited) + \
               constant * math.sqrt(2.0 * math.log1p(parent.visited) / children.visited)

        maxNodes = []
        maxActions = []
        currentMaxValue = 0.0
        # print mctsNode.children
        for action in mctsNode.children:
            children = mctsNode.children[action]
            currentValue = selection_formula(mctsNode, children, constant)
            if currentMaxValue < currentValue:
                maxNodes = [children]
                maxActions = [action]
                # print "select action: ", action
                currentMaxValue = currentValue
            elif currentMaxValue == currentValue:
                maxNodes.append(children)
                maxActions.append(action)
        index = random.randint(0, len(maxNodes) - 1)
        action = maxActions[index]
        self.state = self.state.generatePacmanSuccessor(action)
        if self.state is None or self.state.isWin() or self.state.isLose():
            return None
        return maxNodes[index]

    def defaultPolicy(self, state, rootState):
        i = 0
        while not state.isLose() and i != 5:
		thr[i] = threading.Thread(target=simulation,args=(self,state,rootState))
		thr[i].start
		i = i+1
	for i in range(5):
		thr[i].join() 

    def simulation():
	actions = state.getLegalPacmanActions(self,state,rootState)
        successor = state.generatePacmanSuccessor(
                actions[random.randint(0, len(actions) - 1)])
        if successor is None or successor.isLose():
                thr[i].current_thread.join()
        state = successor
        
        return normalizedScoreEvaluation(rootState, state)

    
    def backPropagation(self, v, r):
        while v is not None:
            v.visited += 1
            v.reward += r
            v = v.parent

    # return actionsList
    def bestChildPolicy(self, root):
        bestActions = []
        maxVisited = 0
        for child in root.children:
            if root.children[child] is None:
                continue
            if root.children[child].visited > maxVisited:
                bestActions = [child]
                maxVisited = root.children[child].visited
            elif root.children[child].visited == maxVisited:
                bestActions.append(child)
        return bestActions

    # GetAction Function: Called with every frame
    def getAction(self, state):
        self.root = MCTSNode(None)
        self.state = state

        vl = self.treePolicy(self.root)
        while vl is not None:  # within computational budget
            reward = self.defaultPolicy(self.state, state)
            if self.state is None:
                break
            self.backPropagation(vl, reward)
            self.state = state
            vl = self.treePolicy(self.root)

        bestActions = self.bestChildPolicy(self.root)
        print state
        if bestActions is []:
            actions = state.getLegalPacmanActions()
            return actions[random.randint(0, len(actions) - 1)]
        bestAction = bestActions[random.randint(0, len(bestActions) - 1)]
        # self.root = self.root.children[bestAction]
        # self.root.parent = None
        # self.clearNode(self.root)
        return bestAction
